# Hardened Firefox user.js

This a file for Firefox that hardens the about:config settings for privacy and security. The main purpose of this is to prevent browser fingerprinting, telemetry and other privacy invasive things.

This is not all you need for privacy. There are still extensions you should use. See https://theprivacyguide1.github.io/browsers.html#firefox for my guide on hardening Firefox for privacy.

An explanation of each setting can be found [here](https://theprivacyguide1.github.io/about_config.html)

## How to use it

Enter about:support in the URL bar in Firefox. In the box that says "Profile Directory" click "Open Directory". You need to place the user.js file in this directory. Reboot Firefox and the settings should be set.

## Mozilla.cfg

You can make a file in your profile directory called mozilla.cfg. In this you can add this user.js but you can change user_pref to lockPref. This means that once you set the settings it cannot be changed back unless you edit mozilla.cfg. This can make it hard to debug something.